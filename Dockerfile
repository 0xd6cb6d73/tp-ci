FROM python:3.7-slim-bullseye 

COPY ./code /opt

WORKDIR /opt

RUN pip install virtualenv
RUN virtualenv -p python3 venv
RUN . venv/bin/activate && pip install -r requirements.txt && echo 'y' | python manage.py seed_db 

CMD ["python", "wsgi.py"]
